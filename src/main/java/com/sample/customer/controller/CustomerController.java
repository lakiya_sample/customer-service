package com.sample.customer.controller;

import com.sample.customer.model.Customer;
import com.sample.customer.model.dto.CustomerCreateDTO;
import com.sample.customer.repository.CustomerRepository;
import com.sample.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("customer")
public class CustomerController {
    private final CustomerRepository customerRepository;
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerRepository customerRepository, CustomerService customerService) {
        this.customerRepository = customerRepository;
        this.customerService = customerService;
    }

    @GetMapping
    public List<Customer> getAllCustomers() {
        return this.customerRepository.findAll();
    }

    @GetMapping("search")
    public List<Customer> searchCustomersByPartialFirstName(@RequestParam String firstName) {
        return this.customerRepository.findByFirstNameContains(firstName);
    }

    @GetMapping("{customerId}")
    public Customer getCustomerById(@PathVariable Long customerId) {
        return this.customerRepository.findById(customerId).orElse(null);
    }

    @PostMapping
    public ResponseEntity<Customer> createCustomer(@RequestBody CustomerCreateDTO customerCreateDTO) {
        Customer dbCustomer = this.customerService.saveCustomer(customerCreateDTO);
        return new ResponseEntity<>(dbCustomer, HttpStatus.CREATED);
    }

    @DeleteMapping("{customerId}")
    public void deleteCustomer(@PathVariable Long customerId) {
        this.customerRepository.deleteById(customerId);
    }

    @PutMapping
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) {
        Customer dbCustomer = this.customerService.updateCustomer(customer);
        return new ResponseEntity<>(dbCustomer, HttpStatus.CREATED);
    }
}

