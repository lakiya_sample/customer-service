package com.sample.customer.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "CUSTOMER")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(accessMode = ApiModelProperty.AccessMode.READ_ONLY, notes = "This is an Auto Generated Value! Keep it blank")
    private Long id;

    @ApiModelProperty(name = "First Name")
    @Column(name = "FIRST_NAME")
    private String firstName;

    @ApiModelProperty(name = "Last Name")
    @Column(name = "LAST_NAME")
    private String lastName;

    @ApiModelProperty(name = "Address")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "id")
    private Address address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
