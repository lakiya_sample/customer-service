package com.sample.customer.service;

import com.sample.customer.model.Customer;
import com.sample.customer.model.dto.CustomerCreateDTO;
import org.springframework.stereotype.Component;

@Component
public interface CustomerService {
    Customer updateCustomer(Customer customer);

    Customer saveCustomer(CustomerCreateDTO customerCreateDTO);
}
